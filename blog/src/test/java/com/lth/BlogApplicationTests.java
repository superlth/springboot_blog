package com.lth;

import com.lth.dao.UserRepository;
import com.lth.po.User;
import com.lth.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BlogApplication.class)
@Transactional
@Rollback
public class BlogApplicationTests {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserService userService;

	@Test
	public void testImpl(){
		User user = userService.checkUser("刘腾辉", "111111");
		System.out.println(user.toString());
	}
}
