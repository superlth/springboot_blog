package com.lth.service;

import com.lth.po.User;

public interface UserService {

    User checkUser(String username, String password);
}
