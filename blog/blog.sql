/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50528
Source Host           : 127.0.0.1:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2020-03-18 21:45:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hibernate_sequence
-- ----------------------------
INSERT INTO `hibernate_sequence` VALUES ('11');
INSERT INTO `hibernate_sequence` VALUES ('11');
INSERT INTO `hibernate_sequence` VALUES ('11');
INSERT INTO `hibernate_sequence` VALUES ('11');
INSERT INTO `hibernate_sequence` VALUES ('11');

-- ----------------------------
-- Table structure for t_blog
-- ----------------------------
DROP TABLE IF EXISTS `t_blog`;
CREATE TABLE `t_blog` (
  `id` bigint(20) NOT NULL,
  `appreciation` bit(1) NOT NULL COMMENT '赞赏',
  `commentabled` bit(1) NOT NULL COMMENT '评论',
  `content` longtext,
  `create_time` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `first_picture` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL COMMENT '标签',
  `published` bit(1) NOT NULL,
  `recommend` bit(1) NOT NULL,
  `share_statement` bit(1) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK292449gwg5yf7ocdlmswv9w4j` (`type_id`),
  KEY `FK8ky5rrsxh01nkhctmo7d48p82` (`user_id`),
  CONSTRAINT `FK292449gwg5yf7ocdlmswv9w4j` FOREIGN KEY (`type_id`) REFERENCES `t_type` (`id`),
  CONSTRAINT `FK8ky5rrsxh01nkhctmo7d48p82` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_blog
-- ----------------------------
INSERT INTO `t_blog` VALUES ('2', '', '', '# 开源项目学习\r\n\r\n这是我的内容', '2020-03-17 15:32:45', '新的开始', 'https://upload.jianshu.io/collections/images/1861958/25279801_1378348357337.jpg?imageMogr2/auto-orient/strip|imageView2/1/w/240/h/240', '原创', '', '', '', 'SpringBoot项目学习', '2020-03-17 15:33:42', '8', '1', '1');
INSERT INTO `t_blog` VALUES ('3', '', '', '# 来一发', '2020-03-17 15:40:35', '就一发', 'https://upload.jianshu.io/collections/images/1861958/25279801_1378348357337.jpg?imageMogr2/auto-orient/strip|imageView2/1/w/240/h/240', '原创', '', '', '', '今日说法', '2020-03-17 15:41:32', '2', '1', '1');
INSERT INTO `t_blog` VALUES ('10', '', '\0', '# 又一发\r\n爽一发', '2020-03-17 15:53:29', '莫得感情', 'https://upload.jianshu.io/collections/images/1861958/25279801_1378348357337.jpg?imageMogr2/auto-orient/strip|imageView2/1/w/240/h/240', '转载', '', '', '\0', '爽歪歪', '2020-03-17 15:54:16', '12', '1', '2');

-- ----------------------------
-- Table structure for t_blog_tags
-- ----------------------------
DROP TABLE IF EXISTS `t_blog_tags`;
CREATE TABLE `t_blog_tags` (
  `blogs_id` bigint(20) NOT NULL,
  `tags_id` bigint(20) NOT NULL,
  KEY `FK5feau0gb4lq47fdb03uboswm8` (`tags_id`),
  KEY `FKh4pacwjwofrugxa9hpwaxg6mr` (`blogs_id`),
  CONSTRAINT `FKh4pacwjwofrugxa9hpwaxg6mr` FOREIGN KEY (`blogs_id`) REFERENCES `t_blog` (`id`),
  CONSTRAINT `FK5feau0gb4lq47fdb03uboswm8` FOREIGN KEY (`tags_id`) REFERENCES `t_tag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_blog_tags
-- ----------------------------
INSERT INTO `t_blog_tags` VALUES ('3', '3');
INSERT INTO `t_blog_tags` VALUES ('10', '3');
INSERT INTO `t_blog_tags` VALUES ('10', '9');

-- ----------------------------
-- Table structure for t_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_comment`;
CREATE TABLE `t_comment` (
  `id` bigint(20) NOT NULL,
  `admin_comment` bit(1) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `blog_id` bigint(20) DEFAULT NULL,
  `parent_comment_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKke3uogd04j4jx316m1p51e05u` (`blog_id`),
  KEY `FK4jj284r3pb7japogvo6h72q95` (`parent_comment_id`),
  CONSTRAINT `FK4jj284r3pb7japogvo6h72q95` FOREIGN KEY (`parent_comment_id`) REFERENCES `t_comment` (`id`),
  CONSTRAINT `FKke3uogd04j4jx316m1p51e05u` FOREIGN KEY (`blog_id`) REFERENCES `t_blog` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_comment
-- ----------------------------
INSERT INTO `t_comment` VALUES ('4', '', 'https://unsplash.it/100/100?image=1005', '自己的评论', '2020-03-17 15:34:54', 'liu1620207158@163.com ', 'admin', '2', null);
INSERT INTO `t_comment` VALUES ('5', '', 'https://unsplash.it/100/100?image=1005', '自己的回复', '2020-03-17 15:35:14', 'liu1620207158@163.com ', 'admin', '2', '4');
INSERT INTO `t_comment` VALUES ('6', '', 'https://unsplash.it/100/100?image=1005', '在来回复', '2020-03-17 15:35:26', 'liu1620207158@163.com ', 'admin', '2', '5');
INSERT INTO `t_comment` VALUES ('7', '', 'https://unsplash.it/100/100?image=1005', '好啊', '2020-03-17 15:35:57', 'liu1620207158@163.com ', '李逍遥', '2', '6');
INSERT INTO `t_comment` VALUES ('8', '', 'https://unsplash.it/100/100?image=1005', '你好', '2020-03-17 15:36:25', 'liu1620207158@163.com ', '紫霞', '2', '7');

-- ----------------------------
-- Table structure for t_tag
-- ----------------------------
DROP TABLE IF EXISTS `t_tag`;
CREATE TABLE `t_tag` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_tag
-- ----------------------------
INSERT INTO `t_tag` VALUES ('3', '学习日志');
INSERT INTO `t_tag` VALUES ('9', '记坑笔记');

-- ----------------------------
-- Table structure for t_type
-- ----------------------------
DROP TABLE IF EXISTS `t_type`;
CREATE TABLE `t_type` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_type
-- ----------------------------
INSERT INTO `t_type` VALUES ('1', 'springBoot');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` bigint(20) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'http://pic2.zhimg.com/50/v2-fb824dbb6578831f7b5d92accdae753a_hd.jpg', '2020-03-17 17:36:04', 'liu1620207158@163.com ', 'admin', '96e79218965eb72c92a549dd5a330112', '1', '2020-03-17 12:36:23', 'admin');
INSERT INTO `t_user` VALUES ('2', 'http://pic2.zhimg.com/50/v2-fb824dbb6578831f7b5d92accdae753a_hd.jpg', '2020-03-17 15:49:50', 'liu1620207158@163.com', 'admin', '96e79218965eb72c92a549dd5a330112', '1', '2020-03-17 15:50:20', '刘腾辉');
